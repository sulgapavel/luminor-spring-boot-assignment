# README #

## PROJECT

* made by Pavel Sulga (11.08.2020)
* email: pavelaizen@gmail.com
* linkedin: https://www.linkedin.com/in/psulga/
* goal: demonstrate technical skills and approach while completing assignment

## Assignment information:
All points except tests and Notification

"Imagine other (micro)service should be notified about the fact we saved valid TYPE1 or TYPE2 payment.
The fact that external service was (un-)successfully notified should be saved to our database."

are fulfilled.

## FROM MY SIDE

*Turned out to be really interesting task, never dealt with GeoLocation API, for example.
*Tried to follow SOLID principles, design patterns and best practices

## HOW TO RUN AN APPLICATION (executable jar)

* Runnable Jar file with all dependecies is created and I will attach it to the email.
* Place jar to some location, run cmd -> move to this location -> execute: "java -jar homework-0.0.1-SNAPSHOT.jar" without quotes.

## LIVE APP ON A REMOTE HOST FOR A TESTING PURPOSES

* App is Idling, to Start it up need to make any rest call, then it will take 20-30s to start Spring boot and receive a response.
* https://morning-lake-00690.herokuapp.com
* Host: Heroku (as it provides a free dubscription option)
* No need to specify port
* An example is:
* https://morning-lake-00690.herokuapp.com/api/payment/payments - to retrieve all payments from DB (consider App is using H2 Database and DB is empty on initialization)

## Data Example to pass as a JSON

* To create Payment
*{
   "creditor_bank_bic_code":"EEBC202",
   "currency":"EUR",
   "debtor_iban":"EE220022710ASD",
   "creditor_iban":"EE22002271340ASD",
   "amount": 771,
   "details":"TEST DETAILS"
*}

## REST APIs DOCUMENTATION

* [GET] - /api/payment/payments - Get all payments from Database
* [GET] - /api/payment/{id} - Get payment by specifying ID
* [GET] - /api/payment/{id}/getCancelationFee - Get payments cancelation fee (if payment is still active, then cancelationFee = null)
* [POST] - /api/payment/type/1 - Create and save new Payment of TYPE1 (EUR)
* [POST] - /api/payment/type/2 - Create and save new Payment of TYPE2 (USD)
* [POST] - /api/payment/type/3 - Create and save new Payment of TYPE3 (EUR or USD)
* [PUT] - /api/payment/{id} - Cancel an active payment and calculate cancelation fee
* [GET] - /api/payment/payments/active - Get all active payments
* [GET] - /api/payment/payments/canceled - Get all canceled payments



## Searching, Sorting, Filtering and Pagination

* page={variable}
* order=asc OR desc
* sort={variable}

* /api/payment/payments/active?page=0 - Return first(0) page with a capacity of 10 objects
* /api/payment/payments/canceled?page=1 - Return second(1) page with a capacity of 10 objects
* /api/payment/payments/active?sort=amount&order=asc - Return first page of payments, sorted by amount in the Ascending order
* /api/payment/payments/canceled?sort=amount&order=desc - Return first page of payments, sorted by amount in the Descening order
* /api/payment/payments/active?sort=id&order=desc - Return first page of payments sorted by id in the descending order
* /api/payment/payments/active - Returns active payments (possible to apply sorting,pagination and filtering)
* /api/payment/payments/canceled - Returns canceled payments (possible to apply sorting,pagination and filtering)

## Technologies, libraries and frameworks used

* Gradle
* Spring Data JPA
* Spring Data REST
* Spring Web
* Spring Boot
* Hibernate
* H2 Embedded Database
* Lombok
* JUnit
* Mockito
* IPGeoLocation API (for determining the Country from the IP address) (requires access key which is passed directly into the method as a "magic number")
* Logger

## H2 Database

* Data is being presented from static variables
* Database is being initialized on the program start from using Hibernate

## Access H2 Database ->
* URL: localhost:8080/h2-console
* username: sa
* password: <empty>

## Unit Tests

* Started writing unit tests but run out of time so left Controller method tests commented out

## Logger

* Logger is writing to log.txt file which locates in the root folder

## THE END
