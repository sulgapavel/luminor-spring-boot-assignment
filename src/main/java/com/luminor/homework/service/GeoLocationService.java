package com.luminor.homework.service;

import io.ipgeolocation.api.Geolocation;
import io.ipgeolocation.api.GeolocationParams;
import io.ipgeolocation.api.IPGeolocationAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GeoLocationService {

    @Value("${geo.location.apy.key}")
    private String api_key;

    private IPGeolocationAPI api = new IPGeolocationAPI(api_key);

    private GeolocationParams geoParams = new GeolocationParams();


    /* Functionality for geolocation check */
    public String checkGeoLocation(String ip) {

        geoParams.setIPAddress(ip);
        Geolocation geolocation = api.getGeolocation(geoParams);

        String countryName;

        if (geolocation.getStatus() == 200){
            countryName = geolocation.getCountryName();
        } else {
            countryName = "Country_Undefined";
        }

        return countryName;
    }
}
