package com.luminor.homework.service;

import com.luminor.homework.model.entity.Payment;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface PaymentService {

    List<Payment> getAllPayments();
    List<Long> findAllActivePayments(Pageable pageable);
    List<Long> findAllCanceledPayments(Pageable pageable);
    void cancelPayment(Long id);
    Optional<Payment> findById(Long id);
    Payment save(Payment payment);

}
