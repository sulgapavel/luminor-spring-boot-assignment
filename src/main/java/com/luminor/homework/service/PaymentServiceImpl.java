package com.luminor.homework.service;

import com.luminor.homework.model.entity.*;
import com.luminor.homework.repository.PaymentRepository;
import com.luminor.homework.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    @Override
    public List<Long> findAllActivePayments(Pageable pageable) {
        return paymentRepository.findByIsActive(pageable);
    }

    @Override
    public List<Long> findAllCanceledPayments(Pageable pageable) {
        return paymentRepository.findByIsCanceled(pageable);
    }

    @Override
    public Payment save(Payment payment){
        return paymentRepository.save(payment);
    }

    @Override
    public Optional<Payment> findById(Long id){
        return paymentRepository.findById(id);
    }


    /* Key functionality for payment cancelation */
    @Override
    public void cancelPayment(Long id){

        Optional<Payment> payment = paymentRepository.findById(id);

        if (paymentCancelationDateCheck(id)){
            payment.orElse(null ).setActive(false);
        } else {
            payment.orElse(null).setActive(true);
        }
        setCancelationFee(id);
    }

    /* Setting cancelation fee*/
    private void setCancelationFee(Long id){
        Optional<Payment> payment = paymentRepository.findById(id);
        assert payment.orElse(null) != null;
        payment.orElse(null).setCancelation_fee(calculateCancelationFee(id));
    }


    /* Method for checking if payment is eligable for cancelation */
    private boolean paymentCancelationDateCheck(Long id){
       Optional<Payment> payment =  paymentRepository.findById(id);
        assert payment.orElse(null) != null;
        return LocalDateTime.now().getDayOfMonth() < payment.orElse(null).getCreated().getDayOfMonth() + 1;
    }

    /* Calculating cancelation fee based on some requirements */
    private double calculateCancelationFee(Long id){

        Optional<Payment> payment =  paymentRepository.findById(id);
        double cancelFee = 0;

        assert payment.orElse(null) != null;

        if (payment.orElse(null).getType().equals("TYPE1")){
            cancelFee = payment.get().getCreated().getHour() * Constant.TYPE1_PAYMENT_CANCELATION_COEFFICIENT;
        }
        else if (payment.get().getType().equals("TYPE2")){
            cancelFee = (payment.get().getCreated().getHour() * Constant.TYPE2_PAYMENT_CANCELATION_COEFFICIENT) * Constant.EUR_TO_USD_EXCHANGE_RATE;
        }
        else if(payment.get().getType().equals("TYPE3") && payment.get().getCurrency() == Currency.USD){
            cancelFee = payment.get().getCreated().getHour() * Constant.TYPE3_PAYMENT_CANCELATION_COEFFICIENT * Constant.EUR_TO_USD_EXCHANGE_RATE;;
        } else if(payment.get().getType().equals("TYPE3") && payment.get().getCurrency() == Currency.EUR){
            cancelFee = payment.get().getCreated().getHour() * Constant.TYPE3_PAYMENT_CANCELATION_COEFFICIENT;
        }
        return cancelFee;
    }
}
