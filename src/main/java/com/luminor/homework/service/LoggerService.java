package com.luminor.homework.service;

import com.luminor.homework.controller.rest.PaymentController;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

@Service
public class LoggerService {

    /* Decided to configure Logger straight away programmaticly so moved it out into its own service */

    private FileHandler handler = new FileHandler("log.txt", true);
    private Logger logger = Logger.getLogger(PaymentController.class.getName());

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-7s] %5$s %n");
    }

    public LoggerService() throws IOException {
    }

    public void loggerSetup(){
        logger.addHandler(handler);
        handler.setFormatter(new SimpleFormatter());
    }

}
