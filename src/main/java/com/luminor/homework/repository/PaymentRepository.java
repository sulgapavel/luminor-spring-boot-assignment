package com.luminor.homework.repository;

import com.luminor.homework.model.entity.Payment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface PaymentRepository extends JpaRepository<Payment, Long> {

    @Query("select p.id from Payment p where p.isActive = true")
    List<Long> findByIsActive(Pageable pageable);

    @Query("select p.id from Payment p where p.isActive = false")
    List<Long> findByIsCanceled(Pageable pageable);
}
