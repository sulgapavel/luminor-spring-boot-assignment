package com.luminor.homework.controller.rest;

import com.luminor.homework.model.entity.Payment;
import com.luminor.homework.model.entity.PaymentType1;
import com.luminor.homework.model.entity.PaymentType2;
import com.luminor.homework.model.entity.PaymentType3;
import com.luminor.homework.model.entity.dto.PaymentDTO;
import com.luminor.homework.repository.PaymentRepository;
import com.luminor.homework.service.GeoLocationService;
import com.luminor.homework.service.LoggerService;
import com.luminor.homework.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping("api/payment")
public class PaymentController {

    private Logger logger = Logger.getLogger(PaymentController.class.getName());

    @Autowired
    private LoggerService loggerService;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentService paymentServiceImpl;

    @Autowired
    private GeoLocationService geoLocationService;

    public PaymentController() throws IOException {
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/payments")
    public List<Payment> getAllPayments(HttpServletRequest request){
        loggerService.loggerSetup();
        logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " + " accessing " + "api/payment/payments");
        return paymentServiceImpl.getAllPayments();
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/{id}")
    public ResponseEntity<Optional<Payment>> getPaymentById(@PathVariable(value = "id") Long paymentId, HttpServletRequest request){
        loggerService.loggerSetup();
        final Optional<Payment> payment = paymentServiceImpl.findById(paymentId);
        assert payment.orElse(null) != null;
        logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " + " accessing " + "api/payment/" + payment.orElse(null).getId());
        return ResponseEntity.ok(payment);
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/{id}/getCancelationFee")
    public ResponseEntity<PaymentDTO> getPaymentIdAndCancelationFee(@PathVariable(value = "id") Long paymentId,HttpServletRequest request){
        loggerService.loggerSetup();
        final Optional<Payment> payment = paymentServiceImpl.findById(paymentId);

        PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.setCancelation_fee(payment.get().getCancelation_fee());
        paymentDTO.setId(paymentId);

        logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " +" accessing " + "api/payment/" + paymentDTO.getId() + "/getCancelationFee");

        return new ResponseEntity<>(paymentDTO, HttpStatus.OK);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping("/type/1")
    public ResponseEntity<Payment> createPaymentType1(@Valid @RequestBody PaymentType1 payment, HttpServletRequest request) {
        loggerService.loggerSetup();

        final Payment result = paymentServiceImpl.save(payment);
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}")
                .buildAndExpand(result.getId()).toUri();

        logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " +" accessing " + "api/payment/type/1");

        return ResponseEntity.created(location).build();
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping("/type/2")
    public ResponseEntity<Payment> createPaymentType2(@Valid @RequestBody PaymentType2 payment,HttpServletRequest request) {
        loggerService.loggerSetup();
        final Payment result = paymentServiceImpl.save(payment);
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}")
                .buildAndExpand(result.getId()).toUri();

        logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " +" accessing " + "api/payment/type/2");

        return ResponseEntity.created(location).build();
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping("/type/3")
    public ResponseEntity<Payment> createPaymentType3(@Valid @RequestBody PaymentType3 payment,HttpServletRequest request) {

        loggerService.loggerSetup();

        final Payment result = paymentServiceImpl.save(payment);
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}")
                .buildAndExpand(result.getId()).toUri();
        logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " +" accessing " + "api/payment/type/3");

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Payment> cancelPayment(@PathVariable(value = "id") Long paymentId,HttpServletRequest request) throws Exception {

        loggerService.loggerSetup();

        Payment payment = paymentServiceImpl.findById(paymentId)
                .orElseThrow(() -> new Exception("Payment not found on :: "+ paymentId));

        paymentServiceImpl.cancelPayment(paymentId);

        final Payment updatedPayment = paymentServiceImpl.save(payment);

        logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " +" accessing " + "api/payment/" + payment.getId());
        return ResponseEntity.ok(updatedPayment);
    }

    @GetMapping("/payments/{isActive}")
    public List<Long> findActive(@PathVariable("isActive")String isActive,
                                 @RequestParam Optional<Integer> page,
                                 @RequestParam(required = false, defaultValue = "Asc") String order,
                                 @RequestParam Optional<String> sort,
                                 HttpServletRequest request){

        loggerService.loggerSetup();

        List<Long> listOfIds = null;

        if (isActive.equals("active")){
            listOfIds = paymentServiceImpl.findAllActivePayments(
                    PageRequest.of(page.orElse(0),10,
                            Sort.Direction.fromString(order), sort.orElse("amount")));
            logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " +" accessing " + "api/payment/payments/active");
        } else if (isActive.equals("canceled")){
           listOfIds = paymentServiceImpl.findAllCanceledPayments(
                    PageRequest.of(page.orElse(0),10,
                            Sort.Direction.fromString(order), sort.orElse("amount")));
            logger.info(request.getRemoteAddr() + " [" + geoLocationService.checkGeoLocation(request.getRemoteAddr()) + "] " +" accessing " + "api/payment/payments/canceled");
        }
        return listOfIds;
    }

}
