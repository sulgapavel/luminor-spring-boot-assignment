package com.luminor.homework.util;

public class Constant {
    // Some constants are moved out to util.Constant class
    // private constructor to hide the implicit public one
    private Constant() {
        throw new IllegalStateException(MESSAGE_UTILITY_CLASS);
    }

    public static final String MESSAGE_UTILITY_CLASS = "Utility class";
    public static final Double TYPE1_PAYMENT_CANCELATION_COEFFICIENT = 0.05D;
    public static final Double TYPE2_PAYMENT_CANCELATION_COEFFICIENT = 0.1D;
    public static final Double TYPE3_PAYMENT_CANCELATION_COEFFICIENT = 0.15D;
    public static final Double EUR_TO_USD_EXCHANGE_RATE = 1.17D;
}
