package com.luminor.homework.model.entity;

public enum Currency {

    EUR, USD
}
