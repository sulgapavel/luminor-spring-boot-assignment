package com.luminor.homework.model.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentDTO {

    private Long id;
    private Double cancelation_fee;

}
