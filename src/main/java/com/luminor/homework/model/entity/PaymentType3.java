package com.luminor.homework.model.entity;

import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@Validated
public class PaymentType3 extends Payment {

    @NotNull
    private String creditor_bank_bic_code;
    @NotNull
    private Currency currency;
    @NotNull
    private String type = "TYPE3";

    @Override
    public void setCurrency(Currency currency){
        if (currency == Currency.EUR || currency == Currency.USD)
        this.currency = currency;
    }

    public PaymentType3(String creditor_bank_bic_code){
        super();
        this.creditor_bank_bic_code = creditor_bank_bic_code;
    }

}
