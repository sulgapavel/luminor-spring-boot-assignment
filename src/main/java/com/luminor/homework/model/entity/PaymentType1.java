package com.luminor.homework.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@Validated
public class PaymentType1 extends Payment {

    @NotNull
    private String details;
    @NotNull
    private Currency currency = Currency.EUR;
    @NotNull
    private String type = "TYPE1";

    public PaymentType1(String details){
        super();
        this.details = details;
    }

}
