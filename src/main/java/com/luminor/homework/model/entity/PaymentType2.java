package com.luminor.homework.model.entity;

import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@Validated
public class PaymentType2 extends Payment {

    private String details;
    @NotNull
    private Currency currency = Currency.USD;
    @NotNull
    private String type = "TYPE2";

    public PaymentType2(String details){
        super();
        this.details = details;
    }

}
