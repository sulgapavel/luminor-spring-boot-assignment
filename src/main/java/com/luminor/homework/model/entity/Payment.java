package com.luminor.homework.model.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name = "payment")
@Valid
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private boolean isActive = true;

    @Setter(AccessLevel.NONE)
    @NotNull
    private Double amount;

    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime created = LocalDateTime.now();

    @Enumerated(EnumType.STRING)
    private Currency currency;

    @NotNull
    private String debtor_iban;

    @NotNull
    private String type;

    @NotNull
    private String creditor_iban;

    private Double cancelation_fee;

    public void setAmount(Double amount) {

            if (amount >= 0D) {
                this.amount = amount;
            } else {
                this.amount = 0.1D;
            }
    }

    public Payment(Double amount, Currency currency, String debtor_iban, String creditor_iban){
        this.amount = amount;
        this.currency = currency;
        this.debtor_iban = debtor_iban;
        this.creditor_iban = creditor_iban;
    }
}
